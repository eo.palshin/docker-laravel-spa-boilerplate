# Docker Laravel Spa Boilerplate
Это стартовый шаблон SPA с использованием Docker с Laravel в качестве API. Предлагаемый вариант использования: клонирование репозитория, доработка конфигурации под свои нужды и разработка с использованием имеющегося набора контейнеров. В своих проектах я обычно использую Laravel для создания API к фронтенду, но ничего не мешает использовать этот репозиторий и для классических веб-приложений.

## Как установить
```bash
# клонируйте репозиторий с GitLab
mkdir project-dir
cd ./project-dir
git clone git@gitlab.com:eo.palshin/docker-laravel-spa-boilerplate.git .

# удалите данные клонированного репозиторий и создайте собственный
rm -rf ./.git
git init
git remote add origin git@gitlab.com:username/project.git
vim README.md # откредактируйте файл README.md
vim .env # отредактируйте файл .env
git add .
git push -u origin master
```

Теперь установите Laravel и Horizon:
```bash
rm -fr ./backend-app/* # очистите каталог backend-app
docker-compose run composer create-project --prefer-dist laravel/laravel .
docker-compose run composer require predis/predis
docker-compose run composer require laravel/horizon
docker-compose run artisan horizon:install
```

Выполните сборку и запуск контейнеров:
```bash
docker-compose up -d
```

Проверьте: теперь по адресу http://localhost:8000 (порт зависит от настроек в файле .env) должна отобразиться приветственная страница Laravel.

Откройте http://localhost:8184 и проверьте, правильно ли работает PHPMyAdmin.


Проверьте, что соединение с базой успешно установлено:
```bash
docker-compose run artisan tinker
>>> DB::connection()->getDatabaseName();
=> "laravel-spa-boilerplate__mysql"
```

Проверьте, что Redis работает:
```bash
docker-compose run artisan tinker
>>> use Illuminate\Support\Facades\Redis;
>>> Redis::set('foo', 'bar');
>>> Redis::get('foo') === 'bar';
=> true
```

Теперь можете создать задачу (Job) и убедиться, что Horizon отрабатывает ее (http://localhost:8000/horizon):

```bash
docker-compose run artisan make:job TestJob
```

Например:
```php
public function handle() {
  Storage::put('test-' . time() . '.log', 'timestamp:' . time());
}
```

Теперь:
```bash
docker-compose run artisan tinker
>>> dispatch(new \App\Jobs\TestJob);
```

И убедитесь, что в файл в каталоге ```storage/app``` создался.